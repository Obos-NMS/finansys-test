
import requests
import pytest

@pytest.fixture
def login_data():
    return {
        "username": "system",
        "password": "P@ssw0rd123#"
    }

@pytest.fixture
def login_url():
    return "https://demo.finansysapps.id/api/identity/authentication/authenticate"

def test_login_successful(login_url, login_data):
    headers = {"Content-Type": "application/json"}

    response = requests.post(login_url, headers=headers, json=login_data)

    assert response.status_code == 200
    # assert "error" in response.json()

def test_login_failed(login_url):
    headers = {"Content-Type": "application/json"}
    invalid_data = {
        "username": "invalid_username",
        "password": "invalid_password"
    }

    response = requests.post(login_url, headers=headers, json=invalid_data)

    assert response.status_code == 200
    assert response.json()["message"] == "Username is not found"