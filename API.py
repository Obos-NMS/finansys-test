import requests

#Login
url = "https://demo.finansysapps.id/api/identity/authentication/authenticate"
headers = {"Content-Type": "application/json"}
data = {"username":"system", "password":"P@ssw0rd123#"}

response = requests.post(url, headers=headers, json=data)

print(response.status_code)
print(response.text)
print("=============")

url = 'https://demo.finansysapps.id/api/identity/authentication/isauthenticated'  # Remove the extra slash after "/api"
headers = {"Content-Type": "application/json"}
response = requests.get(url)
print(response.status_code)
print(response.text)




#Check List Application With Scema
url = 'https://demo.finansysapps.id/api/form/Editor/ListApplications'  # Remove the extra slash after "/api"
# headers = {"Content-Type": "application/json", "Authorization":"Bearer ",{response.json.token}}
headers = {"Content-Type": "application/json", "Authorization":"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6InN5c3RlbSIsInJvbGUiOiJBZG1pbiIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvdXNlcmRhdGEiOiJ7XCJPcmdhbml6YXRpb25JZFwiOlwiZWU4ZDRkZTQ4NWIyNGE4MDlkNjFhMjhlY2ZhYzE1MjBcIixcIlByaW1hcnlUZWFtSWRcIjpcIjI2NzJlNjU0YTA5NzRlYjk5ZmY5ZjQ0YmJkNWQ5NzhmXCIsXCJBcHBVc2VySWRcIjpcIjhkOTJmYjVjYzZhYTQ5YThhNDQ4NzMzZDg2OTE3YTQ0XCIsXCJVc2VybmFtZVwiOlwic3lzdGVtXCIsXCJGdWxsbmFtZVwiOlwiU3lzdGVtIEFkbWluaXN0cmF0b3JcIixcIkVtYWlsXCI6XCJzeXN0ZW11c2VyQGZpbmFuc3lzYXBwcy5jb21cIixcIlRva2VuRXhwaXJ5XCI6XCIyMDIzLTA2LTIyVDAzOjE0OjUxXCIsXCJJc1N5c0FkbWluXCI6dHJ1ZSxcIlRlbmFudENvZGVcIjpcImRlbW9cIixcIkFjY2Vzc1Rva2VuXCI6XCIwQzBDMDM0Q0M0NDQ0RUEwQjY2RkVDRUY5MzdGMzkxQlwiLFwiQ29ubmVjdGlvblN0cmluZ1wiOlwiRTd6MHR6SXVsdCtScGxJNm5BOEJkbjQycW03dlYzM1FYbnFmMHBITmExM1p0V1M1aDNKalE5OHkyK0k2Y0h1cFRVN0pEcnhkMUo2OHJ2Q0RyZXFNV0dRS3pXY0JnWTY1VDZ2cHRhcXpZUVNGbGEwbVYvVzFCbTdVTFY3SnQxbU1MaDlzNmJNRzdIQXRkQXdBUXR4bGRQOEZvTUorZSszMUw0ZmdWcll4R0s1bms1cFQ2VEkvTjVEWHA0aVVXYjNlUXd5aUJDdTVHdmlEQ0Y0cHhsU05mR1dxYjg4dURZTENnM05rWWpFc3RTaFk0dlhrZWNBM1pLbXhYTm5SSGduQ2RiN0ZycVR6byt3c0R4cEFjQTc3VktmbDZ0Y1B5MGJOOWlGNFNibUd3clE9XCJ9IiwibmJmIjoxNjg3NDAwMDkxLCJleHAiOjE2ODc0MTA4OTEsImlhdCI6MTY4NzQwMDA5MX0.uB7AKICm8eHWFdmPFmeIyLxeJvkyYgJXdOaw6n8s5FY"}

response = requests.get(url, headers=headers)

if response.status_code == 200:  # Assuming a successful response
    response_data = response.json()  # Parse the response content as JSON

    # Verify the application object schema
    expected_schema = {
        "application_module_category_id": "",
        "category_name": "Demonstration",
        "created_by": "e1e349999fdd4966bd111c71d9fb123b",
        "created_on": "2021-10-26T06:46:57.426434",
        "display_name": "Sales Invoice Demo",
        "id": "2df4062c330845e7b5462b39aed721ee",
        "module_name": "sales_invoice_demo"
    }
    print(response.content)
    print('=======================================================================================')
    application_object = response_data  # Modify this based on the actual structure of the response

    # Compare the application object with the expected schema
    if application_object == expected_schema:
        print("Application object matches the expected schema.")
    else:
        print("Application object does not match the expected schema.")
else:
    print("GET request failed with status code:", response.status_code)
