<h2>Tata Cara Menjalankan Automation Test Dengan Pytest / Python </h2>

<ul>Untuk Menjalankan Program Automation Interface
<li>Install terlebih dahulu Python https://www.python.org/downloads/</li>
<li>Setelah terinstall python, tulis perintah "pip install"</li>
<li>Untuk Menjalankan automation API tersedia di file API.py, TC.py, da TC1.py dengan perintah "python <namafile>" atau "pytest <namafile>"
<li>Sedangkan untuk Testing Selenium bisa dengan command "pytest Selenium.py"
</ul>
