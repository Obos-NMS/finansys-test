import time
import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

@pytest.fixture
def setup():
    driver = webdriver.Chrome()
    yield driver
    driver.quit()

@pytest.mark.created_application_with_duplicated_name
def test_created_application_with_duplicated_name(setup):
    driver = setup

    driver.get("https://demo.finansysapps.id/")

    # Enter valid username and password
    name_input = driver.find_element(By.NAME, "email")
    name_input.send_keys("system")
    time.sleep(3)

    password_input = driver.find_element(By.ID, "password")
    password_input.send_keys("P@ssw0rd123#")
    time.sleep(3)

    # Submit the form
    password_input.send_keys(Keys.RETURN)

    time.sleep(10)

    # Check if the redirect is successful
    current_url = driver.current_url
    expected_url = "https://demo.finansysapps.id/tasks"
    assert current_url == expected_url, f"Redirect failed. Expected: {expected_url}, Actual: {current_url}"

    driver.get("https://demo.finansysapps.id/form-editor")
    time.sleep(10)

    button_app = driver.find_element(By.XPATH, "//*[@id='kt_content']/div[2]/div/div/div[1]/div[1]/div/button")
    button_app.click()
    time.sleep(5)

    element = driver.find_element(By.NAME, "name")
    element.send_keys("Testing")
    time.sleep(2)
    
    element2 = driver.find_element(By.XPATH, "//textarea[@name='description']")
    element2.send_keys("description")
    time.sleep(5)

    submit = driver.find_element(By.XPATH, "//button[@type='submit']")
    submit.click()
    time.sleep(7)

    asserted = driver.find_element(By.XPATH, "//*[@id='kt_body']/div[6]/div/div/div/div")
    assert asserted.is_displayed()
